import React from 'react';

const SideBar = () => {
    return (
        <div className='border-l border-white h-1/2 my-auto px-6 space-y-4'>
            <img src="/_images/home.svg" alt="home" />
        </div>
    );
}

export default SideBar;
